# Frontend Mentor - Results summary component solution

This is a solution to the [Results summary component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/results-summary-component-CE_K6s0maV). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the interface depending on their device's screen size
- See hover and focus states for all interactive elements on the page
- **Bonus**: Use the local JSON data to dynamically populate the content

### Screenshot

![Mobile](./screenshots/mobile.jpg)
![Desktop](./screenshots/desktop.jpg)

### Links

- Solution URL: [Gitlab Repository](https://gitlab.com/frontend-mentor-challenge/results-summary-component)
- Live Site URL: [Live](https://results-summary-component-2ld2.onrender.com)

## My process

Use ::before pseudo-element to position icon.

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow
- [Google Fonts](https://fonts.google.com/) - Fonts
- [SASS](https://sass-lang.com/) - SASS

### What I learned

To implement the circle with gradient, use linear-gradient, border-radius, and flexbox.

To see how you can add code snippets, see below:

```css
.porcent {
    margin: 2.4rem auto;
    border-radius: 50%;
    background: linear-gradient(180deg, #4D21C9 0%, rgba(37, 33, 201, 0.00) 100%, rgba(37, 33, 201, 0.00) 100%);
    width: 14rem;
    height: 14rem;
    display: flex;
    align-items: center;
    justify-content: center;
}
```
### Continued development

Implement data from JSON file.

### Useful resources

- [Figma](https://figma.com) - For design

## Author

- Website - [Issac Leyva](In construction)
- Frontend Mentor - [@issleyva](https://www.frontendmentor.io/profile/issleyva)

## Acknowledgments